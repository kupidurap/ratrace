# Makefile

CC=gcc
CFLAGS=-Wall -std=gnu99

all: ratrace client

ratrace: linked_list.c ratrace.c utils.c
	gcc -std=gnu11 linked_list.c ratrace.c utils.c -o ratrace -lpthread -D_GNU_SOURCE
client: client.c
	gcc -o client client.c -lpthread
