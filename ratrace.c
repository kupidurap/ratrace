#define _GNU_SOURCE 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <signal.h>
#include <pthread.h>

#include "utils.h"
#include "ratrace.h"

volatile sig_atomic_t work = 1;

void generate_and_send_broadcast();

void
siginthandler(int sig)
{
	work = 0;
}

void 
usage(char *name)
{
	fprintf(stderr, "USAGE: %s dictionary\n",name);
	exit(EXIT_FAILURE);
}

void
lock_player(player* pinfo)
{
	if (pthread_mutex_lock(&pinfo->mutex) != 0)
		ERR("pthread_mutex_lock");
}

void
unlock_player(player* pinfo)
{
	if (pthread_mutex_unlock(&pinfo->mutex) != 0)
		ERR("pthread_mutex_unlock");
}

void
lock_game(game* ginfo)
{
	if (pthread_mutex_lock(&ginfo->mutex) != 0)
		ERR("pthread_mutex_lock");
}

void
unlock_game(game* ginfo)
{
	if (pthread_mutex_unlock(&ginfo->mutex) != 0)
		ERR("pthread_mutex_unlock");
}

//-------------------Player registration and communication----------------------
int 
is_login_unique(char login[NMMAX+1])
{
	int is_unique = 1;
	linked_list_node *cur;
	
	if (!registered_players->count)
		return is_unique;

	for (cur = registered_players->head; cur != NULL; cur = cur->next)
	{
		player *info = (player*)cur->data;
		
		if (!strcmp(info->login, login))
			is_unique = 0;
	}

	return is_unique;
}

void
free_game(game* ginfo)
{
	pcleanup(&ginfo->mutex, &ginfo->cond);
	free(ginfo);
}

void 
add_player_games(player *pinfo)
{
	linked_list_node *cur;
	
	if (!registered_players->count)
		return;

	for(cur = registered_players->head; cur != NULL; cur = cur->next)
	{
		player *opponent = (player*)cur->data;

		game *new_game = (game*)malloc(sizeof(game));
		if (new_game == NULL)
			ERR("malloc");
		
		new_game->first = pinfo;
		new_game->second = opponent;

		new_game->finished = 0;
		new_game->winner = NULL;

		pthread_mutexattr_t attr;
		if (pthread_mutexattr_init(&attr) != 0)
			ERR("pthread_mutexattr_init");
		if (pthread_mutex_init(&new_game->mutex, &attr) != 0)
			ERR("pthread_mutex_init");

		pthread_condattr_t attr_cond;
		if (pthread_condattr_init(&attr_cond) != 0)
			ERR("pthread_condattr_init");
		if (pthread_cond_init(&new_game->cond, &attr_cond) != 0)
			ERR("pthread_cond_init");

		generate_word_list(new_game);

		safe_append_node(scheduled_games, (void*)new_game);
	}
}

void
generate_word_list(game *new_game) {
	new_game->sentence.count = random_between(1, MAX_WORDS_IN_GAME);
	int last_generated = -1;

	for (int i = 0; i < new_game->sentence.count; i++)
		{
			int word_index = random_at_most(MAX_DICTIONARY - 1);

			if (last_generated == word_index)
			{
				i--;
				continue;
			}

			last_generated = word_index;
			strcpy(new_game->sentence.words[i], dictionary[word_index]);
		}
}

player*
add_player(char* login, int clientfd)
{
	player *pinfo = (player*)malloc(sizeof(player));
	if (pinfo == NULL)
		ERR("malloc");

	strcpy(pinfo->login, login);
	pinfo->busy = 0;
	pinfo->broadcast = 0;
	pinfo->score = 0;
	pinfo->played_rivals = create_linked_list();
	pinfo->current_game = NULL;
	pinfo->lost = 0;

	pthread_mutexattr_t attr;
	if (pthread_mutexattr_init(&attr) != 0)
		ERR("pthread_mutexattr_init");
	if (pthread_mutex_init(&pinfo->mutex, &attr) != 0)
		ERR("pthread_mutex_init");

	pthread_condattr_t attr_cond;
	if (pthread_condattr_init(&attr_cond) != 0)
		ERR("pthread_condattr_init");
	if (pthread_cond_init(&pinfo->cond, &attr_cond) != 0)
		ERR("pthread_cond_init");

	lock_list(registered_players);
	add_player_games(pinfo);
	unlock_list(registered_players);

	safe_append_node(registered_players, (void*)pinfo);
	safe_append_node(all_players, (void*)pinfo);

	return pinfo;
}

int
register_player_or_disconnect(int clientfd, player** registered_player)
{
	ssize_t size;
	char login[NMMAX];
	char buffer[CHUNKSIZE];
	int is_unique;

	if ((size = read(clientfd, login, NMMAX)) <= 0)
		ERR("read");

	if (size > 0)
	{
		is_unique = is_login_unique(login);
		
		if (is_unique) {
			*registered_player = add_player(login, clientfd);
			printf("%s registered\n", login);
		}

		sprintf(buffer, 
				is_unique ? REGISTER_CORRECT_STRING : REGISTER_FAILURE_STRING);
				
		if (TEMP_FAILURE_RETRY(send(clientfd, buffer, CHUNKSIZE, 0)) == -1)
			ERR("write");

		if (!is_unique)
		{	
			if (TEMP_FAILURE_RETRY(close(clientfd)) < 0)
				ERR("close");
			return -1;
		}
	}
	
	return 0;
}

int
participates_in_game(player *pinfo, game* ginfo)
{
	return (pinfo == ginfo->first) || (pinfo == ginfo->second);
}

void
print_schedules_game()
{
	linked_list_node *current = scheduled_games->head;

	while (current != NULL) {
		game *current_game = (game*) current->data;
		printf("%s vs %s\n", current_game->first->login, current_game->second->login);
		current = current->next;
	}
}

void
deregister_player(player *pinfo)
{
	remove_node(registered_players, pinfo);
	linked_list_node *previous = NULL;
	linked_list_node *current = scheduled_games->head;

	while (current != NULL) {
		game *current_game = (game*) current->data;
		if (participates_in_game(pinfo, current_game)) {
			if (scheduled_games->head == current) scheduled_games->head = current->next;
			if (scheduled_games->tail == current) scheduled_games->tail = previous;

			if (previous != NULL) previous->next = current->next;

			//todo zwolnienie pamieci dla usuwanej gry
			free(current);
			scheduled_games->count--;
		}

		previous = current;
		current = current->next;
	}
}

void 
communicate(int clientfd, communication_thread_arg *targ)
{
	player *pinfo;
	int disconnected = 0;

	if (register_player_or_disconnect(clientfd, &pinfo) == -1)
		return;

	while(work)
	{
		lock_player(pinfo);
		while ((pinfo->current_game == NULL) && !pinfo->broadcast && work)
			if (pthread_cond_wait(&pinfo->cond, &pinfo->mutex) != 0)
				ERR("pthread_cond_wait");
		unlock_player(pinfo);

		if (pinfo->broadcast)
		{
			if (bulk_write(clientfd, (void *) pinfo->broadcast_message, CHUNKSIZE) < 0)
				ERR("write");
			pinfo->broadcast = 0;
			continue;
		}

		int word_index = 0;
		while(!pinfo->lost && pinfo->current_game != NULL) {
			char buf[MAX_WORD];
			if (bulk_write(clientfd,
						   (void *) pinfo->current_game->sentence.words[word_index], MAX_WORD) < 0)
				ERR("write");

			ssize_t read_size = read(clientfd, buf, MAX_WORD);
			if (read_size < 0)
				ERR("read");
			else if (read_size == 0) {
				disconnected = 1;
				break;
			}
			else {
				if (pinfo->lost)
					break;

				if (!strcmp(buf, ""))
					continue;

				if (!strcmp(buf, pinfo->current_game->sentence.words[word_index]))
				{
					word_index++;
				}

				if (word_index == pinfo->current_game->sentence.count)
				{
					printf("%s won!\n", pinfo->login);
					fflush(stdout);
					break;
				}
			}
		}

		if(disconnected)
		{
			remove_node(registered_players, pinfo);
			deregister_player(pinfo);

			lock_player(pinfo);
			lock_game(pinfo->current_game);

			pinfo->current_game->finished = 1;
			if (pinfo->current_game->first == pinfo)
				pinfo->current_game->winner = pinfo->current_game->second;
			else
				pinfo->current_game->winner = pinfo->current_game->first;

			if (pthread_cond_signal(&pinfo->current_game->cond) != 0)
				ERR("pthread_cond_signal");
			unlock_game(pinfo->current_game);
			unlock_player(pinfo);
			break;
		}

		if (pinfo->lost)
		{
            char buf[CHUNKSIZE];
            strcpy(buf, "!You lost.");
            if (bulk_write(clientfd, (void *) buf, CHUNKSIZE) < 0)
                ERR("write");
			printf("%s lost...\n", pinfo->login);
			fflush(stdout);
			continue;
		}

		if (!pinfo->busy)
			continue;

		if (!pinfo->current_game)
			continue;

		lock_player(pinfo);
		lock_game(pinfo->current_game);
		pinfo->current_game->finished = 1;
		pinfo->busy = 0;
		pinfo->current_game->winner = pinfo;

        char buf[CHUNKSIZE];
        strcpy(buf, "!You won.");
        if (bulk_write(clientfd, (void *) buf, CHUNKSIZE) < 0)
            ERR("write");

		if (pthread_cond_signal(&pinfo->current_game->cond) != 0)
			ERR("pthread_cond_signal");
		unlock_game(pinfo->current_game);
		pinfo->current_game = NULL;
		unlock_player(pinfo);
	}

	if (TEMP_FAILURE_RETRY(close(clientfd)) < 0)
		ERR("close");
}

void 
cleanup(void *arg)
{
	if (pthread_mutex_unlock((pthread_mutex_t *)arg) != 0)
		ERR("pthread_mutex_unlock");
}

void*
communication_thread_func(void *arg)
{
	int clientfd;
	communication_thread_arg targ;

	if (memcpy(&targ, arg, sizeof(targ)) == NULL)
		ERR("memcpy");

	while (1)
	{
		pthread_cleanup_push(cleanup, (void *) targ.mutex);
		if (pthread_mutex_lock(targ.mutex) != 0)
			ERR("pthread_mutex_lock");
		(*targ.idlethreads)++;
		while (!*targ.condition && work)
			if (pthread_cond_wait(targ.cond, targ.mutex) != 0)
				ERR("pthread_cond_wait");
		*targ.condition = 0;
		if (!work)
			pthread_exit(NULL);
		(*targ.idlethreads)--;
		clientfd = *targ.socket;
		pthread_cleanup_pop(1);
		communicate(clientfd, &targ);
	}

	return NULL;
}

void 
init(pthread_t *thread, communication_thread_arg *targ, pthread_cond_t *cond,
		pthread_mutex_t *mutex, int *idlethreads, int *socket, int *condition)
{
	int i;

	for (i = 0; i < PLAYER_NUM; i++)
	{
		targ[i].id = i + 1;
		targ[i].cond = cond;
		targ[i].mutex = mutex;
		targ[i].idlethreads = idlethreads;
		targ[i].socket = socket;
		targ[i].condition = condition;
		if (pthread_create(&thread[i], NULL, communication_thread_func, (void *) &targ[i]) != 0)
			ERR("pthread_create");
	}
}

//------------------------Game scheduling thread--------------------------------
void*
scheduling_thread_func(void *arg)
{
	scheduling_thread_arg targ;
	if (memcpy(&targ, arg, sizeof(targ)) == NULL)
		ERR("memcpy");

	game *my_game = NULL;
	pthread_t thread[GAMES_NUM];
	game_playing_thread_arg targs[GAMES_NUM];

	init_game_threads(thread, targs, targ.cond, &my_game, targ.mutex,
					  targ.idlethreads, targ.condition);

	int need_to_print = 0;

	while(work)
	{
		linked_list_node *i = NULL;

		lock_list(scheduled_games);
		for (i = scheduled_games->head; i != NULL; i = i->next)
		{
			game *possible_game = (game*) i->data;

			lock_player(possible_game->first);
			lock_player(possible_game->second);
			if (!possible_game->first->busy && !possible_game->second->busy)
			{
				unlock_player(possible_game->first);
				unlock_player(possible_game->second);
				if (pthread_mutex_lock(targ.mutex) != 0)
					ERR("pthread_mutex_lock");
				if (*targ.idlethreads == 0)
				{
					if (pthread_mutex_unlock(targ.mutex) != 0)
						ERR("pthread_mutex_unlock");
				}
				else
				{
					need_to_print = 1;
					my_game = possible_game;
					i = i->next;
					remove_node(scheduled_games, possible_game);

					possible_game->first->busy = 1;
					possible_game->second->busy = 1;

					if (pthread_mutex_unlock(targ.mutex) != 0)
						ERR("pthread_mutex_unlock");
					*targ.condition = 1;
					if (pthread_cond_signal(targ.cond) != 0)
						ERR("pthread_cond_signal");

					if (!i)
						break;
				}
				continue;
			}
			unlock_player(possible_game->first);
			unlock_player(possible_game->second);
		}
		unlock_list(scheduled_games);

		if (!need_to_print || !played_all_games()) continue;
		broadcast_ranking();
		need_to_print = 0;
	}

	if (pthread_cond_broadcast(targ.cond) != 0)
		ERR("pthread_cond_broadcast");
	for (int i = 0; i < GAMES_NUM; i++)
		if (pthread_join(thread[i], NULL) != 0)
			ERR("pthread_join");

	return NULL;
}

void
broadcast_ranking() {
	lock_list(registered_players);
	sort_player_list(registered_players);
	unlock_list(registered_players);

	generate_and_send_broadcast();
}

void
generate_and_send_broadcast() {
	char ranks[100];
	generate_ranks(ranks);
	broadcast_results(ranks);
}

void
generate_ranks(char ranks[]) {
	ranks[0] = 0;
	linked_list_node *i = NULL;
    if (strcat(ranks, "!Scores: \n") == NULL)
        ERR("strcat");

	int position = 1;
	for (i = registered_players->head; i != NULL;
			 i = i->next)
		{
			player *pinfo = (player*) i->data;
			char line[15];
			snprintf(line, sizeof(line), "%d. %s %d\n", position++, pinfo->login, pinfo->score);
			if (strcat(ranks, line) == NULL)
				ERR("strcat");
		}
}

void
broadcast_results(char ranks[]) {
	linked_list_node *i = NULL;

	for(i = registered_players->head; i != NULL;
				i = i->next)
		{
			player *pinfo = (player*) i->data;
			lock_player(pinfo);
			strcpy(pinfo->broadcast_message, ranks);
			pinfo->broadcast = 1;
			if (pthread_cond_signal(&pinfo->cond) != 0)
				ERR("pthread_cond_signal");
			unlock_player(pinfo);
		}
}

int
played_all_games() {
	int played_all_games = 1;
	linked_list_node *i = NULL;

	lock_list(registered_players);
	for (i = registered_players->head; i != NULL; i = i->next)
		{
			player *pinfo = (player*) i->data;
			linked_list_node *j = NULL;

			for (j = registered_players->head; j != NULL; j = j->next)
			{
				if (i->data == j->data) continue;
				played_all_games &= contains(pinfo->played_rivals,
											 j->data);
			}
		}
	unlock_list(registered_players);
	return played_all_games;
}

pthread_t
start_game_scheduling_thread(scheduling_thread_arg *starg)
{
	pthread_t ptid;

	if (pthread_create(&ptid, NULL, scheduling_thread_func, (void *) starg) != 0)
		ERR("pthread_create");

	return ptid;
}

//------------------------Game playing thread-----------------------------------
void
play_game(game *current_game, game_playing_thread_arg *targ)
{
	player *first_player = current_game->first;
	player *second_player = current_game->second;

	lock_player(first_player);
	lock_player(second_player);
	first_player->current_game = current_game;
	first_player->busy = 1;
	first_player->lost = 0;
	second_player->current_game = current_game;
	second_player->busy = 1;
	second_player->lost = 0;
	if (pthread_cond_signal(&second_player->cond) != 0)
		ERR("pthread_cond_signal");
	if (pthread_cond_signal(&first_player->cond) != 0)
		ERR("pthread_cond_signal");
	unlock_player(second_player);
	unlock_player(first_player);

	printf("%s vs %s\n", first_player->login, second_player->login);
	lock_game(current_game);
	while(!current_game->finished && work)
		if (pthread_cond_wait(&current_game->cond, &current_game->mutex))
			ERR("ptrhead_cond_wait");
	unlock_game(current_game);

	lock_player(first_player);
	lock_player(second_player);
	if (current_game->winner == first_player)
	{
		first_player->score++;
		second_player->lost = 1;
	}
	else
	{
		second_player->score++;
		first_player->lost = 1;
	}

	append_node(first_player->played_rivals, second_player);
	append_node(second_player->played_rivals, first_player);

	first_player->busy = 0;
	first_player->current_game = NULL;
	second_player->busy = 0;
	second_player->current_game = NULL;

	unlock_player(first_player);
	unlock_player(second_player);

	free_game(current_game);
}

void*
game_thread_func(void *arg)
{
	game *my_game;
	game_playing_thread_arg targ;

	if (memcpy(&targ, arg, sizeof(targ)) == NULL)
		ERR("memcpy");

	while (1)
	{
		pthread_cleanup_push(cleanup, (void *) targ.mutex);
		if (pthread_mutex_lock(targ.mutex) != 0)
			ERR("pthread_mutex_lock");

		(*targ.idlethreads)++;
		while (!*targ.condition && work)
			if (pthread_cond_wait(targ.cond, targ.mutex) != 0)
				ERR("pthread_cond_wait");
		*targ.condition = 0;
		if (!work)
			pthread_exit(NULL);
		(*targ.idlethreads)--;
		my_game = *targ.my_game;
		pthread_cleanup_pop(1);
		play_game(my_game, &targ);
	}

	return NULL;
}

void
init_game_threads(pthread_t *thread, game_playing_thread_arg *targ, pthread_cond_t *cond, game** my_game,
	 pthread_mutex_t *mutex, int *idlethreads, int *condition)
{
	int i;

	for (i = 0; i < GAMES_NUM; i++)
	{
		targ[i].id = i + 1;
		targ[i].cond = cond;
		targ[i].mutex = mutex;
		targ[i].idlethreads = idlethreads;
		targ[i].my_game = my_game;
		targ[i].condition = condition;
		if (pthread_create(&thread[i], NULL, game_thread_func, (void *) &targ[i]) != 0)
			ERR("pthread_create");
	}
}
//------------------------Accepting connections---------------------------------
void
accept_connections(int socket, pthread_t *thread, communication_thread_arg *targ,
				   pthread_cond_t *cond, pthread_mutex_t *mutex, int *idlethreads,
				   int *cfd, sigset_t *oldmask, int *condition)
{
	int clientfd;
	fd_set base_rfds, rfds;
	FD_ZERO(&base_rfds);
	FD_SET(socket, &base_rfds);

	while (work)
	{
		rfds = base_rfds;
		if (pselect(socket + 1, &rfds, NULL, NULL, NULL, oldmask) > 0)
		{
			if ((clientfd = add_new_client(socket)) == -1)
				continue;
			if (pthread_mutex_lock(mutex) != 0)
				ERR("pthread_mutex_lock");
			if (*idlethreads == 0)
			{
				if (TEMP_FAILURE_RETRY(close(clientfd)) == -1)
					ERR("close");
				if (pthread_mutex_unlock(mutex) != 0)
					ERR("pthread_mutex_unlock");
			}
			else
			{
				*cfd = clientfd;
				if (pthread_mutex_unlock(mutex) != 0)
					ERR("pthread_mutex_unlock");
				*condition = 1;
				if (pthread_cond_signal(cond) != 0)
					ERR("pthread_cond_signal");
			}
		}
		else
		{
			if (EINTR == errno)
				continue;
			ERR("pselect");
		}
	}
}
//----------------------------------cleanup------------------------------------
void
free_player(player* pinfo)
{
	delete_list(pinfo->played_rivals);
	pcleanup(&pinfo->mutex, &pinfo->cond);
	free(pinfo);
}

void
free_players()
{
	linked_list_node *i = all_players->head;
	while(i != NULL)
	{
		player *pinfo = (player*) i->data;
		free_player(pinfo);
		i = i->next;
	}
}

void 
pcleanup(pthread_mutex_t *mutex, pthread_cond_t *cond)
{
	if (pthread_mutex_destroy(mutex) != 0)
		ERR("pthread_mutex_destroy");
	if (pthread_cond_destroy(cond) != 0)
		ERR("pthread_cond_destroy");
}

//----------------------------------main----------------------------------------
int 
main(int argc, char **argv)
{
	int i, condition = 0, socket, new_flags, cfd, idlethreads = 0;
	int g_condition = 0, g_idlethreads = 0;
	pthread_t thread[PLAYER_NUM];
	communication_thread_arg targ[PLAYER_NUM];
	pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
	pthread_cond_t g_cond = PTHREAD_COND_INITIALIZER;
	pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
	pthread_mutex_t g_mutex = PTHREAD_MUTEX_INITIALIZER;

	sigset_t mask, oldmask;

	registered_players = create_linked_list();
	scheduled_games = create_linked_list();
	all_players = create_linked_list();

	if (argc!=2)
		usage(argv[0]);

	sethandler(SIG_IGN, SIGPIPE);
	sethandler(siginthandler, SIGINT);

	i = 0;
	FILE *file = fopen(argv[1], "r");

	if (file == 0)
	{
		fprintf(stderr, "failed to open database\n");
		exit(1);
	}
	while (i < MAX_DICTIONARY && fgets(dictionary[i], sizeof(dictionary[0]), file))
	{
		dictionary[i][strlen(dictionary[i])-1] = '\0';
		i = i + 1;
	}
	fclose(file);

	sigemptyset(&mask);
	sigaddset(&mask, SIGINT);
	sigprocmask(SIG_BLOCK, &mask, &oldmask);
	socket = bind_tcp_socket(3000);
	new_flags = fcntl(socket, F_GETFL) | O_NONBLOCK;
	if (fcntl(socket, F_SETFL, new_flags) == -1)
		ERR("fcntl");
		
	init(thread, targ, &cond, &mutex, &idlethreads, &cfd, &condition);

	scheduling_thread_arg starg;
	starg.cond = &g_cond;
	starg.condition = &g_condition;
	starg.idlethreads = &g_idlethreads;
	starg.mutex = &g_mutex;

	pthread_t scheduler = start_game_scheduling_thread(&starg);

	accept_connections(socket, thread, targ, &cond, &mutex, &idlethreads, &cfd, &oldmask, &condition);
	
	if (pthread_cond_broadcast(&cond) != 0)
		ERR("pthread_cond_broadcast");
	for (i = 0; i < PLAYER_NUM; i++)
		if (pthread_join(thread[i], NULL) != 0)
			ERR("pthread_join");

	if (pthread_join(scheduler, NULL) != 0)
		ERR("pthread_join");

	delete_list(scheduled_games);
	delete_list(registered_players);
	free_players();
	delete_list(all_players);

	pcleanup(&mutex, &cond);
	if (TEMP_FAILURE_RETRY(close(socket)) < 0)
		ERR("close");
		
	return EXIT_SUCCESS;
}
