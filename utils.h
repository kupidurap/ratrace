#include "linked_list.h"

#define BACKLOG 3

#define ERR(source) (perror(source),\
		     fprintf(stderr,"%s:%d\n",__FILE__,__LINE__),\
		     exit(EXIT_FAILURE))

void sethandler(void (*f)(int), int sigNo);

int bulk_read(int fd, char *buf, int count);
int bulk_write(int fd, char *buf, int count);

int make_socket(int domain, int type);
int bind_tcp_socket(int port);
int add_new_client(int sfd);

void sort_player_list(linked_list *list);

int random_at_most(int max);
unsigned int random_between(unsigned int min, unsigned int max);