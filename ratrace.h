#ifndef RATRACE_H
#define RATRACE_H

#define CHUNKSIZE 500
#define NMMAX 10
#define PLAYER_NUM 5
#define GAMES_NUM 20

#define MAX_WORD 10
#define MAX_DICTIONARY 100
#define MAX_WORDS_IN_GAME 5

#define REGISTER_CORRECT_STRING "Registered correctly."
#define REGISTER_FAILURE_STRING "Login must be unique."

typedef struct player
{
	char login[NMMAX + 1];
	int score;
	char busy;

	pthread_mutex_t mutex;
	pthread_cond_t cond;

	struct game *current_game;
	int lost;

	linked_list *played_rivals;
	char broadcast;
	char broadcast_message[CHUNKSIZE];
} player;

void lock_player(player* pinfo);
void unlock_player(player* pinfo);

typedef struct word_dictionary
{
	char words[MAX_WORDS_IN_GAME][MAX_WORD];
	int count;
} word_dictionary;

typedef struct game
{
	player *first;
	player *second;

	int finished;
	player *winner;

	pthread_mutex_t mutex;
	pthread_cond_t cond;

	word_dictionary sentence;
} game;

void lock_game(game* ginfo);
void unlock_game(game* ginfo);

typedef struct
{
	int id;
	int *idlethreads;
	int *socket;
	int *condition;
	pthread_cond_t *cond;
	pthread_mutex_t *mutex;
} communication_thread_arg;

typedef struct
{
	int id;
	int *idlethreads;
	int *condition;
	game **my_game;
	pthread_cond_t *cond;
	pthread_mutex_t *mutex;
} game_playing_thread_arg;

typedef struct
{
	int *idlethreads;
	int *condition;
	pthread_cond_t *cond;
	pthread_mutex_t *mutex;
} scheduling_thread_arg;

//globals

linked_list *all_players;
linked_list *registered_players;
linked_list *scheduled_games;
char dictionary[MAX_DICTIONARY][MAX_WORD+1];

//------------------------------------Player functions-------------------------
int is_login_unique(char login[NMMAX+1]);
void add_player_games(player *pinfo);
player* add_player(char* login, int clientfd);
int register_player_or_disconnect(int clientfd,
								  	player **registered_player);
void generate_word_list(game *new_game);

//------------------------------------Communication----------------------------
void communicate(int clientfd, communication_thread_arg *targ);
void cleanup(void *arg);
void* communication_thread_func(void *arg);
void init(pthread_t *thread, communication_thread_arg *targ,
			pthread_cond_t *cond, pthread_mutex_t *mutex, int *idlethreads, 
			int *socket, int *condition);

//------------------------------------Game scheduling--------------------------
pthread_t start_game_scheduling_thread(scheduling_thread_arg *starg);
void* scheduling_thread_func(void *arg);
int played_all_games();
void broadcast_results(char ranks[]);
void generate_ranks(char ranks[]);
void broadcast_ranking();

//------------------------------------Game functions---------------------------
void init_game_threads(pthread_t *thread, game_playing_thread_arg *targ,
					   pthread_cond_t *cond, game** my_game,
					   pthread_mutex_t *mutex, int *idlethreads,
					   int *condition);
void* game_thread_func(void *arg);
void play_game(game *current_game, game_playing_thread_arg *targ);

//------------------------------------Main functions---------------------------
void accept_connections(int socket, pthread_t *thread,
						communication_thread_arg *targ, pthread_cond_t *cond,
						pthread_mutex_t *mutex, int *idlethreads,
						int *cfd, sigset_t *oldmask, int *condition);

//----------------------------------------Misc---------------------------------
void siginthandler(int sig);
void usage(char *name);

void pcleanup(pthread_mutex_t *mutex, pthread_cond_t *cond);

#endif
