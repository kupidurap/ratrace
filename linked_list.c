#include "linked_list.h"
#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

linked_list* create_linked_list()
{
	linked_list *list = (linked_list*)malloc(sizeof(linked_list));
	if (list == NULL)
		ERR("malloc");

	list->head = NULL;
	list->tail = NULL;
	list->count = 0;

	pthread_mutexattr_t attr;
	if (pthread_mutexattr_init(&attr) != 0)
		ERR("pthread_mutexattr_init");
	if (pthread_mutex_init(&list->lock, &attr) != 0)
		ERR("pthread_mutex_init");

	return list;
}

void delete_list(linked_list *list)
{
	if (pthread_mutex_destroy(&list->lock) != 0)
		ERR("pthread_mutex_destroy");

	linked_list_node *current = list->head;

	while(current != NULL)
	{
		linked_list_node *next = current->next;
		free(current);
		current = next;
	}

	free(list);
}

void append_node(linked_list* list, void* data)
{
	linked_list_node *new_node = (linked_list_node*)malloc(sizeof(linked_list_node));
	if (new_node == NULL)
		ERR("malloc");
	
	new_node->data = data;
	new_node->next = NULL;
	
	if (!list->head)
	{
		list->head = new_node;
	}
	else 
	{	
		list->tail->next = new_node;
	}
	
	list->tail = new_node;
	list->count++;
}

void safe_append_node(linked_list *list, void *data)
{
	lock_list(list);
	append_node(list, data);
	unlock_list(list);
}

void remove_node(linked_list *list, void *data) {
    linked_list_node *previous = NULL;
    linked_list_node *current = list->head;

    while (current != NULL) {
        if (current->data == data) {
            if (list->head == current) list->head = current->next;
            if (list->tail == current) list->tail = previous;

            if (previous != NULL) previous->next = current->next;

            free(current);
            list->count--;
            return;
        }

        previous = current;
        current = current->next;
    }
}

int contains(linked_list *list, void *data)
{
	linked_list_node *iterator = NULL;

	for(iterator = list->head; iterator != NULL; iterator = iterator->next)
	{
		if (iterator->data == data)
			return 1;
	}

	return 0;
}

void swap_nodes(linked_list_node *first, linked_list_node *second)
{
	void* temp = first->data;
	first->data = second->data;
	second->data = temp;
}

void lock_list(linked_list *list)
{
	if (pthread_mutex_lock(&list->lock) != 0)
		ERR("pthread_mutex_lock");
}

void unlock_list(linked_list *list)
{
	if (pthread_mutex_unlock(&list->lock) != 0)
		ERR("pthread_mutex_unlock");
}