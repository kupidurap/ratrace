#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <signal.h>
#include <netdb.h>
#include <pthread.h>

#include "utils.h"
#include "ratrace.h"

void 
sethandler(void (*f)(int), int sigNo)
{
	struct sigaction act;
	memset(&act, 0x00, sizeof(struct sigaction));
	act.sa_handler = f;

	if (-1 == sigaction(sigNo, &act, NULL))
		ERR("sigaction");
}

int
bulk_read(int fd, char *buf, int count)
{
	int c;
	size_t len = 0;

	do
	{
		c = TEMP_FAILURE_RETRY(read(fd, buf, count));
		if (c < 0)
			return c;
		if (c == 0)
			return len;
		buf += c;
		len += c;
		count -= c;
	}
	while (count > 0);

	return len;
}

int
bulk_write(int fd, char *buf, int count)
{
	int c;
	size_t len = 0;

	do
	{
		c = TEMP_FAILURE_RETRY(write(fd, buf, count));
		if(c < 0)
			return c;
		buf += c;
		len += c;
		count -= c;
	}
	while (count > 0);

	return len;
}

int 
make_socket(int domain, int type)
{
	int sock;
	sock = socket(domain, type, 0);
	if (sock < 0)
		ERR("socket");

	return sock;
}

int 
bind_tcp_socket(int port)
{
	struct sockaddr_in addr;
	int socketfd, t=1;

	socketfd = make_socket(PF_INET, SOCK_STREAM);
	if (memset(&addr, 0x00, sizeof(struct sockaddr_in)) == NULL)
		ERR("memset");

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);

	if (setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, &t, sizeof(t)))
		ERR("setsockopt");
	if (bind(socketfd, (struct sockaddr *) &addr, sizeof(addr)) < 0)
		ERR("bind");
	if (listen(socketfd, BACKLOG) < 0)
		ERR("listen");

	return socketfd;
}

int 
add_new_client(int sfd)
{
	int nfd;
	if ((nfd = TEMP_FAILURE_RETRY(accept(sfd, NULL, NULL))) < 0)
	{
		if (EAGAIN == errno || EWOULDBLOCK == errno)
			return -1;
		ERR("accept");
	}

	return nfd;
}

void sort_player_list(linked_list *list)
{
	linked_list_node *i, *h = list->head;

	for(i = h; i != NULL && i->next != NULL;
		i = i->next)
	{
		linked_list_node *max = i;
		linked_list_node *j = NULL;

		for(j = i->next; j != NULL; j = j->next)
		{
			player *best_info = (player*)max->data;
			player *candidate_info = (player*)j->data;

			if (best_info->score < candidate_info->score)
				max = j;
		}

		if (max != i)
			swap_nodes(i, max);
	}

	list->head = h;
}

unsigned int random_between(unsigned int min, unsigned int max)
{
	int r;
	const unsigned int range = 1 + max - min;
	const unsigned int buckets = RAND_MAX / range;
	const unsigned int limit = buckets * range;

	do
	{
		r = rand();
	} while (r >= limit);

	return min + (r / buckets);
}

int random_at_most(int max) {
	return random_between(0, max);
}