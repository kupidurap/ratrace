#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <bits/pthreadtypes.h>
//TODO Zwalnianie zasobów listy
typedef struct linked_list_node
{
	void *data;
	struct linked_list_node *next;
} linked_list_node;

typedef struct linked_list
{
	pthread_mutex_t lock;
	struct linked_list_node *head;
	struct linked_list_node *tail;
	int count;
} linked_list;

linked_list* create_linked_list();
void delete_list(linked_list *list);
void append_node(linked_list *list, void *data);
void remove_node(linked_list *list, void *data);
int contains(linked_list *list, void *data);

void lock_list(linked_list *list);
void unlock_list(linked_list *list);
void safe_append_node(linked_list *list, void *data);

void swap_nodes(linked_list_node *first, linked_list_node *second);

#endif 